FROM node:alpine
WORKDIR /app
COPY package*.json ./
EXPOSE 4200
RUN npm clean-install
RUN npm install -g @angular/cli@latest
COPY . .

CMD ng serve --host 0.0.0.0 --disable-host-check

