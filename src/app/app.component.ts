import { Component } from '@angular/core';
import { WebSocketAPI } from './websoketAPI.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'soket-app';

  constructor(private websoket: WebSocketAPI, private http: HttpClient) {}

  connect() {
    this.websoket._connect();
  }

  send() {
    this.websoket._send('test request', '');
  }
}
