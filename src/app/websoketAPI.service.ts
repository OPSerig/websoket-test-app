import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class WebSocketAPI {
  private headers = new HttpHeaders({ Authorization: 'Bearer' + '' });
  webSocketEndPoint: string = environment.backUrl;
  topic: string = '/topic/chat/';
  stompClient: any;
  responseMessage = new Subject<any>();
  sessionId: string;

  constructor() {}
  _connect() {
    console.log('Initialize WebSocket Connection');
    let ws = new SockJS(this.webSocketEndPoint, null, {
      transports: ['xhr-streaming'],
      headers: this.headers
    });
    this.stompClient = Stomp.over(ws);
    const _this = this;
    _this.stompClient.connect(
      {},
      function(frame) {
        let urlarray = ws._transport.url.split('/');
        this.sessionId = urlarray[urlarray.length - 2];
        localStorage.setItem('userUUID', this.sessionId);
        _this.stompClient.subscribe(_this.topic + this.sessionId, function(
          sdkEvent
        ) {
          console.log(sdkEvent.body);

          _this.responseMessage.next(JSON.parse(sdkEvent.body));
        });
      },
      this.errorCallBack
    );
  }

  _disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  errorCallBack(error) {
    console.log('errorCallBack -> ' + error);
    setTimeout(() => {
      this._connect();
    }, 5000);
  }

  /**
   * Send message to sever via web socket
   * @param {*} message
   */
  _send(message: any, rout: string) {
    console.log('calling logout api via web socket');
    this.stompClient.send(rout, this.headers, JSON.stringify(message));
  }
}
